## My first Ansibler

![Image](images/nixon.gif)

### SetUp

```bash
vagrant up
```

```bash
vagrant ssh
mkdir -p /var/www/html
```

```bash
sudo su
export http_proxy=http://proxy.reply.it:8080/
```

```bash
sudo su
apt update
apt upgrade -y
```

```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
sudo apt-get update
sudo apt-get install ansible
```

```bash
ansible-galaxy install geerlingguy
cd /vagrant/vagrant_data/first
ansible-playbook site.yml
```

### Ip of machine

```bash
ping 192.168.33.12
```

### mysql 

```bash
vagrant ssh
mysql -uroot -pfart
```

### go

```bash
sudo nano /etc/hosts
192.168.33.12 local.vagrant
``` 

> [local.vagrant](http://local.vagrant)

### role ansible 

```bash
ls -l /root/.ansible
```

### Teach

> [docs.ansible.com/ansible/latest/installation_guide/intro_installation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

> [docs.ansible.](https://galaxy.ansible.com/geerlingguy)

> [ansible-role-php](https://github.com/geerlingguy/ansible-role-php)

> [install-and-configure-nginx-using-ansible](https://code-maven.com/install-and-configure-nginx-using-ansible)